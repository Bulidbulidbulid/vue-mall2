/**
 * 商品请求
 */
const DOMAIN = 'http://120.78.156.117:8051/';
const HOST = DOMAIN + 'api/goods/';

const URL = {
  shopDetail: HOST + 'detail'
}

export {
  DOMAIN,
  URL
}
