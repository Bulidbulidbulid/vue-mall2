import axios from 'axios';

/**
 * axios get method wrapper
 * @param api
 * @param data
 * @param success
 * @param error
 */

export const GetWrapper = (api, data, success, error) => {
  axios.get(
    api,
    data
  ).then((response) => {
    if (response.code) {
      success(response);
    } else {
      error(response);
    }
  }).catch((e) => {
    console.warn('请求失败：', e);
    error(e);
  })
}

/**
 * axios post method wrapper
 * @param api
 * @param data
 * @param success
 * @param error
 */

export const PostWrapper = (api, data, success, error) => {
  axios.post(
    api,
    data
  ).then((response) => {
    if (response.code) {
      success(response);
    } else {
      error(response);
    }
  }).catch((e) => {
    console.warn('请求失败：', e);
    error(e);
  })
}
