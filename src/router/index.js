import Vue from 'vue'
import Router from 'vue-router'
import detail from '@/views/detail/detail'
import login from '@/views/login/login'
import pact from '@/views/pact/pact'
import comment from '@/views/comment/comment'
import download from '@/views/download/download'
import list from '@/views/list/list'
import articledetail from '@/views/list/detail'
import secondComment from '@/views/list/secondComment'
// import buy from '@/views/buy/buy'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'detai',
      component: detail,
      meta: {
        title: '两只喜鹊'
      }
    },
    {
      path: '/detail',
      name: 'detail',
      component: detail,
      meta: {
        title: '两只喜鹊'
      }
    },
    {
      path: '/login',
      name: 'login',
      component: login,
      meta: {
        title: '两只喜鹊-注册'
      }
    },
    {
      path: '/pact',
      name: 'pact',
      component: pact,
      meta: {
        title: '用户使用协议'
      }
    },
    {
      path: '/comment',
      name: 'comment',
      component: comment,
      meta: {
        title: '两只喜鹊-本地生活'
      }
    },
    {
      path: '/download',
      name: 'download',
      component: download,
      meta: {
        title: '两只喜鹊-下载'
      }
    },
    {
      path: '/list',
      name: 'list',
      component: list,
      meta: {
        title: '两只喜鹊-文章列表'
      }
    },
    {
      path: '/articledetail',
      name: 'articledetail',
      component: articledetail,
      meta: {
        title: '两只喜鹊-文章详情'
      }
    },
    {
      path: '/secondcomment',
      name: 'secondcomment',
      component: secondComment,
      meta: {
        title: '两只喜鹊-评论页'
      }
    // },
    // {
    //   path: '/buy',
    //   name: 'buy',
    //   component: buy,
    //   meta: {
    //     title: '两只喜鹊-下载'
    //   }
    }
  ]
})
