// 公共js文件

/**
 * 验证方法
 */
var verify = {
  phone: (res) => {
    const PHONE_REG = /^[1][3,4,5,6,7,8,9][0-9]{9}$/;
    let result = {
      statu: 0,
      msg: ''
    }
    if (res) {
      if (!PHONE_REG.test(res)) {
        result.statu = 2;
        result.msg = '手机号有误';
      }
    } else {
      result.statu = 1;
      result.msg = '手机号不能为空';
    }
    return result
  },
  strNum: (res, num, val) => {
    let result = {
      statu: 0,
      msg: ''
    }
    val = val || '字符';
    if (res.length < num) {
      result.statu = 1;
      result.msg = '请输入' + num + '位以上的' + val;
    }
    return result;
  }
}
var tool = {
  // 判断是否是微信浏览器的函数
  isWeiXin: function () {
    // window.navigator.userAgent属性包含了浏览器类型、版本、操作系统类型、浏览器引擎类型等信息，这个属性可以用来判断浏览器类型
    var ua = navigator.userAgent.toLowerCase();
    try {
      // 通过正则表达式匹配ua中是否含有MicroMessenger字符串
      if (ua.match(/MicroMessenger/i)[0] === 'micromessenger') {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }
}

export {
  verify,
  tool
}
