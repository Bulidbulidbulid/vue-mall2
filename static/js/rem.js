// ;(function () {
//     /**
//      * 7.5=设计稿尺寸/100
//      * css元素尺寸=设计稿元素尺寸/100;
//      */
//     var change = 'orientationchange' in window ? 'orientationchange' : 'resize';
//     function calculate() {
//         var deviceWidth = document.documentElement.clientWidth;
//         if (deviceWidth < 320) {
//             deviceWidth = 320;
//         } else if (deviceWidth > 640) {
//             deviceWidth = 640;
//         }
//         document.documentElement.style.fontSize = deviceWidth / 6.4 + 'px';
//     };
//     window.addEventListener(change, calculate, false);
//     calculate();
// })();
var clientWidth = document.documentElement.clientWidth;

var fontSize = clientWidth / 7.5 + 'px';
var h = document.getElementsByTagName('html');
h[0].style.fontSize = fontSize;
(function (doc, win) {
  var docEl = doc.documentElement;
  var resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';
  var recalc = function () {
    var clientWidth = docEl.clientWidth;
    if (!clientWidth) return;
    // console.log(clientWidth);
    docEl.style.fontSize = clientWidth / 7.5 + 'px';
  };
  if (!doc.addEventListener) return;
  win.addEventListener(resizeEvt, recalc, false);
  doc.addEventListener('DOMContentLoaded', recalc, false);
})(document, window);
